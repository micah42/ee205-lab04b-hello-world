///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04b - Hello World II
///
/// This is a procedural Hello World program
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
   printf("Hello world!\n");

   printf("This program [%s] was compiled on [%s] at [%s]\n", __FILE__, __DATE__, __TIME__);

   printf("We are in the [%s] function right now.\n", __FUNCTION__);

#ifdef __INTEL_COMPILER
   printf("The Intel compiler was used for compiling this program\n");
#elif __clang__
   printf("The LLVM Clang compiler version [%s] was used for compiling this program\n", __VERSION__);
#elif __GNUC__
   printf("The GNU Compiler version [%s] was used for compiling this program\n", __VERSION__);
#elif _MSC_VER
   printf("The compiler is Microsoft Visual Studio version [%d]\n", _MSC_VER);
#else
   printf("Can not determine the compiler\n");
#endif


#ifdef __cplusplus
   printf("Using a C++ compiler -- compiling to the [%ld] standard\n", __cplusplus);
#else
   printf("Using a C compiler -- compiling to the [%ld] standard\n", __STDC_VERSION__);
#endif

   return 0;
}

